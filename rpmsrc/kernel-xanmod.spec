%define _build_id_links none

%ifarch x86_64
%define karch x86_64
%define karch32 x86
%endif

# whether to use LLVM-built kernel package dependencies
%define llvm_kbuild 0

Name: kernel-xanmod-edge
Summary: The Linux Kernel with XanMod Patches
Version: 5.13.1

%define flaver xm1

Release:%{flaver}.0%{?dist}

%define rpmver %{version}-%{release}
%define krelstr %{release}.%{_arch}
%define kverstr %{version}-%{krelstr}

License: GPLv2 and Redistributable, no modifications permitted
Group: System Environment/Kernel
Vendor: The Linux Community and XanMod maintainer(s)
URL: https://xanmod.org
Source0: kernel-%{version}.tar.gz
# Secure boot signing certificates -- Pulled directly from Fedora DistGit
# only intended for x86_64
Source1: redhatsecurebootca1.cer
Source2: redhatsecurebootca5.cer
Source3: redhatsecureboot301.cer
Source4: redhatsecureboot501.cer
Source5: x509.genkey.fedora
Source6: mod-sign.sh

%define secureboot_ca_0 %{SOURCE1}
%define secureboot_ca_1 %{SOURCE2}
%define secureboot_key_0 %{SOURCE3}
%define secureboot_key_1 %{SOURCE4}
%define pesign_name_0 redhatsecureboot301
%define pesign_name_1 redhatsecureboot501
%define kmodsign_cmd %{SOURCE6}
###
%define __spec_install_post /usr/lib/rpm/brp-compress || :
%define debug_package %{nil}
Source7: rnread-%{name}
BuildRequires: python3-devel make perl-generators perl-interpreter openssl-devel bison flex findutils git-core perl-devel openssl elfutils-devel gawk binutils m4 tar hostname bzip2 bash gzip xz bc diffutils redhat-rpm-config net-tools elfutils patch rpm-build dwarves kmod libkcapi-hmaccalc perl-Carp rsync grubby pesign
%if %{llvm_kbuild}
BuildRequires: llvm lld clang
%else
BuildRequires: gcc
%endif
Requires: %{name}-core-%{rpmver} = %{kverstr}, %{name}-modules-%{rpmver} = %{kverstr}
Provides: rnread-%{name} = 2

%description
The kernel-%{flaver} meta package

%package core
Summary: Kernel core package
Group: System Environment/Kernel
Provides: installonlypkg(kernel), kernel = %{rpmver}, kernel-core = %{rpmver}, kernel-core-uname-r = %{kverstr}, kernel-uname-r = %{kverstr}, kernel-%{_arch} = %{rpmver}, kernel-core%{_isa} = %{rpmver}, kernel-core-%{rpmver} = %{kverstr}, %{name}-core-%{rpmver} = %{kverstr}, kernel-drm-nouveau = 16
Requires: bash, coreutils, dracut, linux-firmware, /usr/bin/kernel-install, kernel-modules-%{rpmver} = %{kverstr}
Supplements: %{name} = %{rpmver}
%description core
The kernel package contains the Linux kernel (vmlinuz), the core of any
Linux operating system.  The kernel handles the basic functions
of the operating system: memory allocation, process allocation, device
input and output, etc.

%package modules
Summary: Kernel modules to match the core kernel
Group: System Environment/Kernel
Provides: installonlypkg(kernel-module), kernel-modules = %{rpmver}, kernel-modules%{_isa} = %{rpmver}, kernel-modules-uname-r = %{kverstr}, kernel-modules-%{_arch} = %{rpmver}, kernel-modules-%{rpmver} = %{kverstr}, %{name}-modules-%{rpmver} = %{kverstr}
Supplements: %{name} = %{rpmver}
%description modules
This package provides kernel modules for the core kernel package.

%package headers
Summary: Header files for the Linux kernel for use by glibc
Group: Development/System
Provides: kernel-headers = %{kverstr}, glibc-kernheaders = 3.0-46, kernel-headers%{_isa} = %{kverstr}
Obsoletes: kernel-headers < %{kverstr}, glibc-kernheaders < 3.0-46
%description headers
Kernel-headers includes the C header files that specify the interface
between the Linux kernel and userspace libraries and programs.  The
header files define structures and constants that are needed for
building most standard programs and are also needed for rebuilding the
glibc package.

%package devel
Summary: Development package for building kernel modules to match the %{kverstr} kernel
Group: System Environment/Kernel
AutoReqProv: no
Requires: findutils perl-interpreter openssl-devel flex make bison elfutils-libelf-devel
%if %{llvm_kbuild}
Requires: llvm lld clang
%else
Requires: gcc
%endif
Enhances: dkms akmods
Provides: installonlypkg(kernel), kernel-devel = %{rpmver}, kernel-devel-uname-r = %{kverstr}, kernel-devel-%{_arch} = %{rpmver}, kernel-devel%{_isa} = %{rpmver}, kernel-devel-%{rpmver} = %{kverstr}, %{name}-devel-%{rpmver} = %{kverstr}
%description -n %{name}-devel
This package provides kernel headers and makefiles sufficient to build modules
against the %{kverstr} kernel package.

%prep
%setup -q -n kernel-%{version}
cp -v %{SOURCE5} certs/x509.genkey

%build
make %{?_smp_mflags} EXTRAVERSION=-%{krelstr}
gcc ./scripts/sign-file.c -o ./scripts/sign-file -lssl -lcrypto

%install

ImageName=$(make image_name | tail -n 1)

mkdir -p %{buildroot}/boot
%ifarch ia64
mkdir -p %{buildroot}/boot/efi
cp -v $ImageName %{buildroot}/boot/efi/vmlinuz-%{krelstr}
ln -s efi/vmlinuz-%{kverstr} %{buildroot}/boot/
%else
%pesign -s -i $ImageName -o vmlinuz.tmp -a %{secureboot_ca_0} -c %{secureboot_key_0} -n %{pesign_name_0}
%pesign -s -i vmlinuz.tmp -o vmlinuz.signed -a %{secureboot_ca_1} -c %{secureboot_key_1} -n %{pesign_name_1}
rm vmlinuz.tmp
mv vmlinuz.signed $ImageName

cp -v $ImageName %{buildroot}/boot/vmlinuz-%{kverstr}
chmod 755 %{buildroot}/boot/vmlinuz-%{kverstr}
%endif

make %{?_smp_mflags} INSTALL_MOD_PATH=%{buildroot} modules_install
make %{?_smp_mflags} INSTALL_HDR_PATH=%{buildroot}/usr headers_install

cp -v System.map %{buildroot}/boot/System.map-%{kverstr}
cp -v .config %{buildroot}/boot/config-%{kverstr}
bzip2 -9 --keep vmlinux
mv vmlinux.bz2 %{buildroot}/boot/vmlinux-%{kverstr}

# prepare -devel files
### most of the things here are derived from the Fedora kernel.spec
### see 
##### https://src.fedoraproject.org/rpms/kernel/blob/rawhide/f/kernel.spec#_1613
### for details
rm -f %{buildroot}/lib/modules/%{kverstr}/build
rm -f %{buildroot}/lib/modules/%{kverstr}/source
mkdir -p %{buildroot}/lib/modules/%{kverstr}/build
mkdir -p %{buildroot}/lib/modules/%{kverstr}/updates
mkdir -p %{buildroot}/lib/modules/%{kverstr}/weak-updates
mkdir -p %{buildroot}/lib/modules/%{kverstr}/build/include/config/
 # Remove testing headers
find . -name "*.h.s" -delete
cp -v --parents $(find -type f -name "Makefile*" -o -name "Kconfig*") %{buildroot}/lib/modules/%{kverstr}/build
if [ ! -e Module.symvers ] ; then
	touch Module.symvers
fi
cp -v Module.symvers %{buildroot}/lib/modules/%{kverstr}/build
cp -v System.map %{buildroot}/lib/modules/%{kverstr}/build
if [ -s Module.markers ] ; then
	cp -v Module.markers %{buildroot}/lib/modules/%{kverstr}/build
fi
gzip -c9 < Module.symvers > %{buildroot}/boot/symvers-%{kverstr}.gz
cp -v %{buildroot}/boot/symvers-%{kverstr}.gz %{buildroot}/lib/modules/%{kverstr}/symvers.gz

rm -rf %{buildroot}/lib/modules/%{kverstr}/build/scripts
rm -rf %{buildroot}/lib/modules/%{kverstr}/build/include
cp -v .config %{buildroot}/lib/modules/%{kverstr}/build
cp -v -a scripts %{buildroot}/lib/modules/%{kverstr}/build
rm -rf %{buildroot}/lib/modules/%{kverstr}/build/scripts/tracing
rm -f %{buildroot}/lib/modules/%{kverstr}/build/scripts/spdxcheck.py

cp -v -a --parents include/generated/uapi/linux %{buildroot}/lib/modules/%{kverstr}/build
mkdir -p %{buildroot}/lib/modules/%{kverstr}/build/security/selinux/include
cp -v -a --parents security/selinux/include/classmap.h %{buildroot}/lib/modules/%{kverstr}/build
cp -v -a --parents security/selinux/include/initial_sid_to_string.h %{buildroot}/lib/modules/%{kverstr}/build
mkdir -p %{buildroot}/lib/modules/%{kverstr}/build/tools/include/tools
cp -v -a --parents tools/include/tools/be_byteshift.h %{buildroot}/lib/modules/%{kverstr}/build
cp -v -a --parents tools/include/tools/le_byteshift.h %{buildroot}/lib/modules/%{kverstr}/build
cp -v -a --parents tools/include/linux/compiler* %{buildroot}/lib/modules/%{kverstr}/build
cp -v -a --parents tools/include/linux/types.h %{buildroot}/lib/modules/%{kverstr}/build
cp -v -a --parents tools/build/Build.include %{buildroot}/lib/modules/%{kverstr}/build
cp -v --parents tools/build/Build %{buildroot}/lib/modules/%{kverstr}/build
cp -v --parents tools/build/fixdep.c %{buildroot}/lib/modules/%{kverstr}/build
cp -v --parents tools/objtool/sync-check.sh %{buildroot}/lib/modules/%{kverstr}/build
cp -v -a --parents tools/bpf/resolve_btfids %{buildroot}/lib/modules/%{kverstr}/build
cp -v --parents security/selinux/include/policycap_names.h %{buildroot}/lib/modules/%{kverstr}/build
cp -v --parents security/selinux/include/policycap.h %{buildroot}/lib/modules/%{kverstr}/build
cp -v -a --parents tools/include/asm-generic %{buildroot}/lib/modules/%{kverstr}/build
cp -v -a --parents tools/include/linux %{buildroot}/lib/modules/%{kverstr}/build
cp -v -a --parents tools/include/uapi/asm %{buildroot}/lib/modules/%{kverstr}/build
cp -v -a --parents tools/include/uapi/asm-generic %{buildroot}/lib/modules/%{kverstr}/build
cp -v --parents tools/scripts/utilities.mak %{buildroot}/lib/modules/%{kverstr}/build
cp -v -a --parents tools/lib/subcmd %{buildroot}/lib/modules/%{kverstr}/build
cp -v --parents tools/lib/*.c %{buildroot}/lib/modules/%{kverstr}/build
cp -v --parents tools/objtool/*.[ch] %{buildroot}/lib/modules/%{kverstr}/build
cp -v --parents tools/objtool/Build %{buildroot}/lib/modules/%{kverstr}/build
cp -v --parents tools/objtool/include/objtool/*.h %{buildroot}/lib/modules/%{kverstr}/build
cp -v -a --parents tools/lib/bpf/Build %{buildroot}/lib/modules/%{kverstr}/build

if [ -f tools/objtool/objtool ] ; then
	cp -v -a tools/objtool/objtool %{buildroot}/lib/modules/%{kverstr}/build/tools/objtool/ || :
fi
if [ -f tools/objtool/fixdep ] ; then
	cp -v -a tools/objtool/fixdep %{buildroot}/lib/modules/%{kverstr}/build/tools/objtool/ || :
fi
if [ -f arch/%{karch}/scripts ] ; then
	cp -v -a arch/%{karch}/scripts %{buildroot}/lib/modules/%{kverstr}/build/arch/%{_arch} || :
fi
if [ -f arch/%{karch}/*lds ] ; then
	cp -v -a arch/%{karch}/*lds %{buildroot}/lib/modules/%{kverstr}/build/arch/%{_arch}/ || :
fi
if [ -f arch/%{karch}/kernel/module.lds ] ; then
	cp -v -a --parents arch/%{karch}/kernel/module.lds %{buildroot}/lib/modules/%{kverstr}/build
fi
if [ -f arch/%{karch32}/scripts ] ; then
	cp -v -a arch/%{karch32}/scripts %{buildroot}/lib/modules/%{kverstr}/build/arch/%{_arch} || :
fi
if [ -f arch/%{karch32}/*lds ] ; then
	cp -v -a arch/%{karch32}/*lds %{buildroot}/lib/modules/%{kverstr}/build/arch/%{_arch}/ || :
fi
if [ -f arch/%{karch32}/kernel/module.lds ] ; then
	cp -v -a --parents arch/%{karch}/kernel/module.lds %{buildroot}/lib/modules/%{kverstr}/build
fi

find %{buildroot}/lib/modules/%{kverstr}/build/scripts \( -iname "*.o" -o -iname "*.cmd" \) -exec rm -f {} +

if [ -d arch/%{karch}/include ] ; then
	cp -v -a --parents arch/%{karch}/include %{buildroot}/lib/modules/%{kverstr}/build/
fi
if [ -d arch/%{karch32}/include ] ; then
	cp -v -a --parents arch/%{karch32}/include %{buildroot}/lib/modules/%{kverstr}/build/
fi


cp -v -a include %{buildroot}/lib/modules/%{kverstr}/build

%ifarch i686 x86_64
	cp -v -a --parents arch/x86/entry/syscalls/syscall_32.tbl %{buildroot}/lib/modules/%{kverstr}/build/
	cp -v -a --parents arch/x86/entry/syscalls/syscalltbl.sh %{buildroot}/lib/modules/%{kverstr}/build/
	cp -v -a --parents arch/x86/entry/syscalls/syscallhdr.sh %{buildroot}/lib/modules/%{kverstr}/build/
	cp -v -a --parents arch/x86/entry/syscalls/syscall_64.tbl %{buildroot}/lib/modules/%{kverstr}/build/
	cp -v -a --parents arch/x86/tools/relocs_32.c %{buildroot}/lib/modules/%{kverstr}/build/
	cp -v -a --parents arch/x86/tools/relocs_64.c %{buildroot}/lib/modules/%{kverstr}/build/
	cp -v -a --parents arch/x86/tools/relocs.c %{buildroot}/lib/modules/%{kverstr}/build/
	cp -v -a --parents arch/x86/tools/relocs_common.c %{buildroot}/lib/modules/%{kverstr}/build/
	cp -v -a --parents arch/x86/tools/relocs.h %{buildroot}/lib/modules/%{kverstr}/build/
	cp -v -a --parents arch/x86/purgatory/purgatory.c %{buildroot}/lib/modules/%{kverstr}/build/
	cp -v -a --parents arch/x86/purgatory/stack.S %{buildroot}/lib/modules/%{kverstr}/build/
	cp -v -a --parents arch/x86/purgatory/setup-x86_64.S %{buildroot}/lib/modules/%{kverstr}/build/
	cp -v -a --parents arch/x86/purgatory/entry64.S %{buildroot}/lib/modules/%{kverstr}/build/
	cp -v -a --parents arch/x86/boot/string.h %{buildroot}/lib/modules/%{kverstr}/build/
	cp -v -a --parents arch/x86/boot/string.c %{buildroot}/lib/modules/%{kverstr}/build/
	cp -v -a --parents arch/x86/boot/ctype.h %{buildroot}/lib/modules/%{kverstr}/build/
 
	cp -v -a --parents tools/arch/x86/include/asm %{buildroot}/lib/modules/%{kverstr}/build
	cp -v -a --parents tools/arch/x86/include/uapi/asm %{buildroot}/lib/modules/%{kverstr}/build
	cp -v -a --parents tools/objtool/arch/x86/lib %{buildroot}/lib/modules/%{kverstr}/build
	cp -v -a --parents tools/arch/x86/lib/ %{buildroot}/lib/modules/%{kverstr}/build
	cp -v -a --parents tools/arch/x86/tools/gen-insn-attr-x86.awk %{buildroot}/lib/modules/%{kverstr}/build
cp -v -a --parents tools/objtool/arch/x86/ %{buildroot}/lib/modules/%{kverstr}/build
%endif

find %{buildroot}/lib/modules/%{kverstr}/build/tools \( -iname "*.o" -o -iname "*.cmd" \) -exec rm -f {} +
touch -r %{buildroot}/lib/modules/%{kverstr}/build/Makefile %{buildroot}/lib/modules/%{kverstr}/build/include/generated/uapi/linux/version.h

find %{buildroot}/lib/modules/%{kverstr} -name "*.ko" -type f >modnames
 
	# mark modules executable so that strip-to-file can strip them

xargs --no-run-if-empty chmod u+x < modnames

 
	# Generate a list of modules for block and networking.
 
grep -F /drivers/ modnames | xargs --no-run-if-empty nm -upA | sed -n 's,^.*/\([^/]*\.ko\):  *U \(.*\)$,\1 \2,p' > drivers.undef

collect_modules_list() {
	sed -r -n -e "s/^([^ ]+) \\.?($2)\$/\\1/p" drivers.undef |
	LC_ALL=C sort -u > %{buildroot}/lib/modules/%{kverstr}/modules.$1
	if [ ! -z "$3" ]; then
		sed -r -e "/^($3)\$/d" -i %{buildroot}/lib/modules/%{kverstr}/modules.$1
	fi
}

collect_modules_list networking 'register_netdev|ieee80211_register_hw|usbnet_probe|phy_driver_register|rt(l_|2x00)(pci|usb)_probe|register_netdevice'
collect_modules_list block 'ata_scsi_ioctl|scsi_add_host|scsi_add_host_with_dma|blk_alloc_queue|blk_init_queue|register_mtd_blktrans|scsi_esp_register|scsi_register_device_handler|blk_queue_physical_block_size' 'pktcdvd.ko|dm-mod.ko'
collect_modules_list drm 'drm_open|drm_init'
collect_modules_list modesetting 'drm_crtc_init'
 
	# detect missing or incorrect license tags

( find %{buildroot}/lib/modules/%{kverstr} -name '*.ko' | xargs /sbin/modinfo -l | grep -E -v 'GPL( v2)?$|Dual BSD/GPL$|Dual MPL/GPL$|GPL and additional rights$' ) && exit 1
 
pushd %{buildroot}/lib/modules/%{kverstr}/
	rm -f modules.{alias*,builtin.bin,dep*,*map,symbols*,devname,softdep}
popd

mkdir -p %{buildroot}/usr/src/kernels
mv %{buildroot}/lib/modules/%{kverstr}/build %{buildroot}/usr/src/kernels/%{kverstr}

ln -sf /usr/src/kernels/%{kverstr} %{buildroot}/lib/modules/%{kverstr}/build
ln -sf /usr/src/kernels/%{kverstr} %{buildroot}/lib/modules/%{kverstr}/source

find %{buildroot}/usr/src/kernels -name "*.*cmd" -delete
#

# Install the Red Hat UEFI Secure Boot CA cert
mkdir -p %{buildroot}/%{_datadir}/doc/kernel-keys/%{kverstr}
%ifarch x86_64 aarch64
	cp -v -ax %{secureboot_ca_0} %{buildroot}/%{_datadir}/doc/kernel-keys/%{kverstr}/kernel-signing-ca-20200609.cer
	cp -v -ax %{secureboot_ca_1} %{buildroot}/%{_datadir}/doc/kernel-keys/%{kverstr}/kernel-signing-ca-20140212.cer
	ln -s kernel-signing-ca-20200609.cer %{buildroot}/%{_datadir}/doc/kernel-keys/%{kverstr}/kernel-signing-ca.cer
%else
	cp -v -ax %{secureboot_ca_0} %{buildroot}/%{_datadir}/doc/kernel-keys/%{kverstr}/kernel-signing-ca.cer
%endif
#

chmod +x %{kmodsign_cmd}
%{kmodsign_cmd} certs/signing_key.pem certs/signing_key.x509 %{buildroot}/lib/modules/%{kverstr}/
cp -v  %{buildroot}/boot/vmlinuz-%{kverstr} %{buildroot}/lib/modules/%{kverstr}/vmlinuz
mkdir -p %{buildroot}/%{_bindir}
cp -v  %{SOURCE7} %{buildroot}/%{_bindir}/rnread-%{name}
chmod u+rwx,go+rx %{buildroot}/%{_bindir}/rnread-%{name}

%clean
rm -rf %{buildroot}

%post core
if [ `uname -i` == "x86_64" -o `uname -i` == "i386" ] &&
   [ -f /etc/sysconfig/kernel ]; then
  /bin/sed -r -i -e 's/^DEFAULTKERNEL=kernel-smp$/DEFAULTKERNEL=kernel/' /etc/sysconfig/kernel || exit $?
fi

%posttrans core
/bin/kernel-install add %{kverstr} /lib/modules/%{kverstr}/vmlinuz || exit $?

### Donation plug
printf "\n\n\n  This package "
printf '%{name}'
printf " is being worked by me as a hobby in my free time\n"
echo -e "  If you're interested in supporting me doing this, you can go to:\n"
echo -e "\t\thttps://liberapay.com/rmnscnce/donate\n\n  And donate so I can keep this package maintained for everyone\n\nYou can read news and announcements for this software using "'rnread-%{name}\n\n\n'

%preun core
/bin/kernel-install remove %{kverstr} /lib/modules/%{kverstr}/vmlinuz || exit $?
if [ -x /usr/sbin/weak-modules ]
then
    /usr/sbin/weak-modules --remove-kernel %{kverstr} || exit $?
fi

%post devel
if [ -f /etc/sysconfig/kernel ]
then
    . /etc/sysconfig/kernel || exit $?
fi
if [ "$HARDLINK" != "no" -a -x /usr/bin/hardlink -a ! -e /run/ostree-booted ] 
then
    (cd /usr/src/kernels/%{kverstr} &&
     /usr/bin/find . -type f | while read f; do
       hardlink -c /usr/src/kernels/*.fc34.*/$f $f
     done)
fi

%post modules
/sbin/depmod -a %{kverstr}

%postun modules
/sbin/depmod -a %{kverstr}

%files core
%defattr (-, root, root)
%exclude /%{kverstr}
/boot/*
%exclude /boot/vmlinux-%{kverstr}
/lib/modules/%{kverstr}/vmlinuz
/lib/modules/%{kverstr}/symvers.gz
%{_datadir}/doc/kernel-keys/%{kverstr}

%files modules
%defattr (-, root, root)
/lib/modules/%{kverstr}
%exclude /lib/modules/%{kverstr}/vmlinuz
%exclude /lib/modules/%{kverstr}/symvers.gz
%exclude /lib/modules/%{kverstr}/build
%exclude /lib/modules/%{kverstr}/source

%files headers
%defattr (-, root, root)
/usr/include

%files devel
%defattr (-, root, root)
/usr/src/kernels/%{kverstr}
/lib/modules/%{kverstr}/build
/lib/modules/%{kverstr}/source

%files
%defattr (-, root, root)
%attr(0755, root, root) %{_bindir}/rnread-%{name}
